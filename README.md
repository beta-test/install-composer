# Install-composer

Install "Composer - Dependency Management for PHP" programmatically. This shell script will install composer automatically on UNIX server.

## Requirements

Git, wget, PHP 5.3.4 or above

## How to use it:
Download the following shell script on your UNIX server or clone it with the following command.

` git clone https://gitlab.com/beta-test/install-composer.git `

### Give the execution right to it.

` sudo chmod +x install-composer/start.sh `

### Then run it as sudo user.

` sudo install-composer/start.sh `